# snapshot-validation-webhook Helm chart

The [snapshot validation webhook](https://github.com/kubernetes-csi/external-snapshotter)
is an optional component for the snapshot-controller. It is an HTTP callback
which responds to [admission requests](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/).
It is part of a larger plan to tighten validation for volume snapshot objects.

Snapshot validation webhook is exposed on an HTTPS endpoint, and so requires
TLS certificate management. This Helm chart relies on [cert-manager.io](https://cert-manager.io/)
to manage the certificate for the webhook.

## Usage

[cert-manager.io](https://cert-manager.io/) must be installed before
installing this Helm chart.

```
helm install <NAME> .
```

## Configuration

Available configuration options:

| Option | Default value | Description |
|--------|---------------|-------------|
| `replicaCount` | `1` | Number of replicas for snapshot-validation-webhook Deployment. Replicas provide active-passive HA through leader election. |
| `image.repository` | `k8s.gcr.io/sig-storage/snapshot-validation-webhook` | snapshot-validation-webhook container image repository. |
| `image.tag` | `v6.0.1` | snapshot-validation-webhook container image tag. |
| `image.pullPolicy` | `IfNotPresent` | Container image pull policy. |
| `tlsCertUpdaterImage.repository` | `alpine` | Image repository for TLS certificate updater sidecar. Needs to have `inotifyd` available in `PATH`. |
| `tlsCertUpdaterImage.tag` | `3.11` | TLS certificate updater sidecar container image tag. |
| `tlsCertUpdaterImage.pullPolicy` | `IfNotPresent` | TLS certificate updater sidecar image pull policy. |
| `validatingWebhook.failurePolicy` | `Fail` | [Failure policy](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#failure-policy) defines the action taken when an admission denial occurs. Allowed values are `Ignore` and `Fail`.
| `validatingWebhook.timeoutSeconds` | `2` | [This timeout](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#timeouts) specifies how long the Kubernetes API server should wait for this webhook to respond before treating the call as a failure.
| `tls.issuerRef` | `{}` | Expects an [`cert-manager.io/v1 ObjectReference`](https://cert-manager.io/docs/reference/api-docs/#meta.cert-manager.io/v1.ObjectReference) If not empty, this issuer will be used to sign the certificate. If none is provided, a new, self-signing issuer will be created.
| `tls.certDuration` | `8760h` | Certificate duration. The generated certificate will be automatically renewed 1/3 of `certDuration` before its expiry. Value must be in units accepted by Go [`time.ParseDuration`](https://golang.org/pkg/time/#ParseDuration). Minimum accepted duration is `1h`. This option may be ignored/overridden by some issuer types.
| `service.port` | `443` | Port on which the HTTPS endpoint of the webhook service will be exposed.
| `serviceAccount.create` | `true` | Specifies whether a service account for snapshot-validation-webhook Deployment should be created. If set to `true`, the service account will have its name set to the value of `serviceAccount.name` |
| `serviceAccount.annotations` | `{}` | Annotations to add to the service account. |
| `serviceAccount.name` | `""` | The name of the service account to use. If not set and `serviceAccount.create` is `true`, a name is generated using the fullname template. |
| `logVerbosityLevel` | `2` | Level of log verbosity. See https://github.com/kubernetes/community/blob/master/contributors/devel/sig-instrumentation/logging.md for description of individual levels. |
