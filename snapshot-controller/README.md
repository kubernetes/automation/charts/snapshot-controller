# snapshot-controller Helm chart

[snapshot-controller](https://github.com/kubernetes-csi/external-snapshotter)
is a cluster-wide component responsible for creating and deleting a snapshot
in the storage system through a CSI volume driver. It is required in order
for snapshotting functionality to work in Kubernetes. It must be deployed
in the cluster exactly once, regardless of the number of CSI drivers installed
in the cluster.

This Helm chart provides deployment the snapshot controller as well as
Custom Resource Definitions for snapshots, snapshot contents, snapshot classes
from the `snapshot.storage.k8s.io/v1` API.

## Usage

Helm supports installing CRDs starting from version 3. Environments with
older versions will need to install snapshot CRDs by other means.

```
helm install <NAME> .
```

## Configuration

Available configuration options:

| Option | Default value | Description |
|--------|---------------|-------------|
| `replicaCount` | `1` | Number of replicas for snapshot-controller Deployment. Replicas provide active-passive HA through leader election. |
| `image.repository` | `k8s.gcr.io/sig-storage/snapshot-controller` | snapshot-controller container image repository. |
| `image.tag` | `v6.0.1` | snapshot-controller container image tag. |
| `image.pullPolicy` | `IfNotPresent` | Container image pull policy. |
| `crds.create` | `true` | Specifies whether to create CRDs required for the snapshot feature to work. |
| `serviceAccount.create` | `true` | Specifies whether a service account for snapshot-controller Deployment should be created. If set to `true`, the service account will have its name set to the value of `serviceAccount.name` |
| `serviceAccount.annotations` | `{}` | Annotations to add to the service account. |
| `serviceAccount.name` | `""` | The name of the service account to use. If not set and `serviceAccount.create` is `true`, a name is generated using the fullname template. |
| `logVerbosityLevel` | `2` | Level of log verbosity. See https://github.com/kubernetes/community/blob/master/contributors/devel/sig-instrumentation/logging.md for description of individual levels. |
