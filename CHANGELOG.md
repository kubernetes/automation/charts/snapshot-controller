# Changelog

## NEXT RELEASE

## v1.2.0

* Add an RBAC rule needed by the controller deployment
* Sync up with github.com/kubernetes-csi/external-snapshotter v6.2.2

## v1.1.0

* Sync up with github.com/kubernetes-csi/external-snapshotter v6.0.1
* Bump images for snapshot-controller and snapshot-validation-webhook to v6.0.1
* Updated VolumeSnapshot CRDs, v1beta1 VolumeSnapshot API no longer served

## v1.0.0

* Added snapshot-controller 1.0.0
* Added snapshot-validation-webhook 1.0.0

Released 2021-04-11
