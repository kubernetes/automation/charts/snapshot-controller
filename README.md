# snapshot-controller Helm charts

This repository contains two Helm charts to enable snapshotting functionality
in a Kubernetes cluster.

* [snapshot-controller](https://github.com/kubernetes-csi/external-snapshotter)
* [snapshot-validation-webhook](https://github.com/kubernetes-csi/external-snapshotter)

See the docs in the respective charts for information about their usage and configuration.
